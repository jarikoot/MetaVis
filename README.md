<div align="center"><img src="www/MetaVis_logo.PNG" alt="MetaVis_logo" width="250"/></div>

MetaVis - Metagenomics Visualization app
================

MetaVis is a free and open source app, designed to create a visualization of shotgun and 16S data. Before you can use this app you need to have your data clustered into OTU's/ASV's. This can be done with the HUMAnN3 (shotgun sequencing data) or the QIIME2 (16S Sequencing data) pipeline.

# Table of Contents

* [Installation](#installation)
* [How to use the app](#how-to-use-the-app)
* [Input data](#input-data)
* [Issues and feedback](#issues-and-feedback)


# Installation

Follow the following steps to clone the MetaVis repository to your local Rstudio.

-   **Option 1** (if Git package is installed): 
1.   Navigate to the folder where you want to have the copy of the MetaVis project.
2.   Copy the code below and paste it in the Rstudio console
3.   Unpack the V4.zip file (packages/V4.zip)

``` R 
git clone https://gitlab.com/jarikoot/mngs-visualisation-app.git
```

-   **Option 2** (without the Git package)
1.   Click on the download button at the [home page of the MetaVis repository](https://gitlab.com/jarikoot/mngs-visualisation-app/-/tree/main) and download the .zip file.  
2.   Unpack the downloaded zip file in the folder where you want to have the copy of the MetaVis project.
3.   Unpack the V4.zip file (packages/V4.zip)



# How to use the app

To open the app, open your Rstudio and navigate to the folder where you downloaded the MetaVis repository. Click on the MetaVis.Rproj file and press "Yes". Then open the ui.R or server.R script and click on "Run App" (top right of the script). The app will now load all the required packages, after this is done the app will launch in a new window. 
It is recommended that you first read all the text in the home screen of the app. Then you can import , filter and visualise your own data or use a sample dataset to see what the app is capable of.


# Input data

For this app to work, you need to have your sequencing data clustered in OTU's/ASV's. This can be done with the HUMAnN3 pipeline (for shotgun sequencing data) or the QIIME2 pipeline (for 16S sequencing data). In total the app you can create a visualisation of the following datasets:

1.   Sample dataset (shotgun sequencing)
2.   Taxonomic output of HUMAnN3 (shotgun sequencing)
3.   Functional output of HUMAnN3 (shotgun sequencing)
4.   Taxonomic output of QIIME2 (16S sequencing)

The total workflow overview is visible in the figure below.

<div align="center"><img src="www/NGS_pipeline.PNG" alt="MetaVis_pipeline" width="600"/></div>


# Issues and feedback

If you run into an issue while using the app or have a question about the way I do the analysis, feel free to open an issue on the [GitLab page](https://gitlab.com/jarikoot/mngs-visualisation-app/-/tree/main#Input_data) of the application. Or you can try fixing it yourself by cloning the GitLab repository and adjusting the code. If you have any feedback on the app you can also drop them on the GitLab site of MetaVis

